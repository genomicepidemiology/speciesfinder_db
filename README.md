SpeciesFinder Database documentation
=============

The SpeciesFinder database is a curated database of acquired resistance genes.

## Content of the repository
1. INSTALL.py - Script for indexing the database with KMA
2. config	- necessary for installing

## Installation
Clone the database
```bash
git clone https://git@bitbucket.org/genomicepidemiology/speciesfinder_db.git
```
The database can be used with BLAST as-is.


### Installing KMA:

#### Download and install KMA
```bash
# Go to the directory in which you want KMA installed
cd /some/path
# Clone KMA
git clone https://bitbucket.org/genomicepidemiology/kma.git
# Go to kma directory and compile code
cd kma && make
```

### Download database
```bash
#Download SILVAN database
wget https://www.arb-silva.de/fileadmin/silva_databases/current/Exports/SILVA_138.1_SSUParc_tax_silva.fasta.gz
gunzip SILVA_138.1_SSUParc_tax_silva.fasta.gz
mv SILVA_138.1_SSUParc_tax_silva.fasta 16srna_database.fasta

### Indexing with *INSTALL.py*
If you have KMA installed you either need to have the kma_index in your PATH or
you need to provide the path to kma_index to INSTALL.py

#### a) Run INSTALL.py in interactive mode
```bash
# Go to the database directory
cd path/to/speciesfinder_db
python3 INSTALL.py
```
If kma_index was found in your path a lot of indexing information will be
printed to your terminal, and will end with the word "done".

If kma_index wasn't found you will recieve the following output:
```bash
KMA index program, kma_index, does not exist or is not executable
Please input path to executable kma_index program or choose one of the options below:
	1. Install KMA using make, index db, then remove KMA.
	2. Exit
```
You can now write the path to kma_index and finish with <enter> or you can
enter "1" or "2" and finish with <enter>.

If "1" is chosen, the script will attempt to install kma in your systems
default temporary location. If the installation is successful it will proceed
to index your database, when finished it will delete the kma installation again.

#### b) Run INSTALL.py in non_interactive mode
```bash
# Go to the database directory
cd path/to/speciesfinder_db
python3 INSTALL.py /path/to/kma_index non_interactive
```
The path to kma_index can be omitted if it exists in PATH or if the script
should attempt to do an automatic temporary installation of KMA.


### Documentation

The documentation available as of the date of this release can be found at:
https://bitbucket.org/genomicepidemiology/speciesfinder_db/overview.


Citation
=======

When using the method please cite:

Not yet published


License
=======

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
